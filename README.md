# oneCHARGE dev code test

oneCHARGE developer interview coding test

You will find few exercises to help us understanding your abilities to code. There are no limit of time, but we would like you to be transparent with us and let us know how long it took to do each section.

Please finish your method in any programming language you are familiar with and try your best to finish as much as you can even you don't know the answer. If you are new to any programming languages, please explain your logic clearly to solve the problem.

We want to see how you use best practices to solve the problem and follow the coding convenstions of your selected languages, most importantly... clean code :)

__Put your works in a git repository or a gist and send us the link (1 file per question).__


### Question 1.

A group of kids (N in total) are about to play a game. The first one starts with saying number 1, and each player thenceforth counts one number in turn. However, if the number is a multiple of 3, the kid should say Fizz instead. Similarly, say Buzz for a number that is multiple of 5 and Woof for any multiples of 7. Given there are N kids, please return the correct responses from them in order.

For example, 
if N is 5, your method returns 1, 2, "Fizz", 4, "Buzz"
if N is 15, your method returns 1, 2, "Fizz", 4, "Buzz", "Fizz", "Woof", 8, "Fizz", "Buzz", 11, "Fizz", 13, "Woof", "Fizz Buzz"

### Question 2.

String reversion:

Given a valid string, please return the reversed version of it. For example, if "xyz123" is the input, your method returns "321zyx". 


### Question 3.

Given a number of coins with different denominations, e.g. [1, 2, 5] and test if they could be used to make up a certain amount (N), assuming you can use unlimited number of coins in each denomination.

For example,
if coins = [1, 2, 5] and N = 11, return true
if coins = [3, 77] and N = 100, return false
